define nuget::package($pkgname, $pkgdest, $version, $ensure=present) {

  $_pkgdest = regsubstr($pkgdest, "/", "\\", "G")

  # by default, install the nuget package
  if ($ensure == present) {

    exec {"nuget install ${pkgname}.${version}":
      command => "${nuget} install -NoCache ${pkgname} -OutputDirectory ${_pkgdest} -Version ${version}",
      require => File["nugetexe"],
      unless => "${::cmdexe} /c dir ${_pkgdest}\\${installdir}",
    }

  }

  # unfortunately, nuget has no concept of uninstall
  # that the best we can do is remove the package installation itself
  else {

    file {"${_pkgdest}/${pkgname}.${version}":
      ensure => absent,
      purge => true,
      recurse => true,
      force => true,
    }

  }

}
