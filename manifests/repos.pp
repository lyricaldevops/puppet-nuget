# This module handles adding and removing nuget repository
# Note: nuget exits 0 whether or not the repository exists, so using findstr is
#  a relatively easy way to determine whether or not the repository is present
define nuget::repo($name, $target, $ensure=present) {

  $_target = regsubstr($target, "/", "\\", "G")

  if ($ensure == present) {

    exec{"adding nuget repository ${name}":
      command => "${nuget} sources Add -Name ${name} -source ${_target}",
      require => File["${nugetexe}"],
      unless => "${cmdexe} /c findstr \"${name}\" ${::appdata}\\Nuget\\Nuget.config",
    }

  }

  elsif ($ensure == absent) {

    exec{"removing nuget repository ${name}":
      command => "${nuget} sources Remove -Name ${name} -source ${_target}",
      require => File["${nugetexe}"],
      onlyif => "${cmdexe} /c findstr \"${name}\" ${::appdata}\\Nuget\\Nuget.config",
    }

  }

}
