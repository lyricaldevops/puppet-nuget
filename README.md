#Puppet-Nuget

This module provides support for [Nuget](http://nuget.org) within Puppet. Puppet-nuget provides support for package installation and removal, and Nuget repository management. Unlike Windows paths, this module supports declaring paths with forward or backward slashes.

License: MIT

## nuget::repo

nuget::repo provides support for adding and removing nuget repository configurations from a host under puppet control. It supports path declarations using forward or backward slashes.

	nuget::repo {"somerepo":
		target => "//fileserver/nuget_storage",
		name => "MyNugetRepo"
		ensure => present # or absent
	}

## nuget::package

nuget::package provides support for installing and removing nuget packages. As nuget does not provide support for centralized package management, or package removal, this module only removes the installed package by removing *pkgdest/pkgname.version* from the file system. 

Note: this module does not read the package manifest for package removal - meaning that it can only remove the installed package, and any additional packages that are installed as a result.

	nuget::package {"mypackage":
		pkgdest => "${::systemdrive}\\Some\\Installation\Directory",
		pkgname => "MyNuget.Package.Name"
		version => "1.0.0.0",
		ensure => present # or absent
	}